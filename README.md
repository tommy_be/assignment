# Assignment

## Requirements
- Python 3.8
- Install dependencies are in `requirements.txt`

## Goal
Predict sentiment of a review (positive/negative)

## Project content
 - working with yelp dataset
 - contains some data exploration
 - contains pipeline to build basic ML model
 - predict sentiment review
 
### Run
 - you need to put the yelp datasets in `datasets` folder, the filenames can be modified in the `Configuration` class
 - you may **limit** number of entries from `datasets`
 - you may **limit** the learning and predictions only specific US `city`
 - run by `python main.py`
