import logging

from pyspark.sql import SparkSession

POSITIVE = 1
NEGATIVE = 0


class Configuration(object):
    def __init__(self):
        logging.getLogger().setLevel(logging.INFO)
        self.dataset_business: str = "datasets/yelp_business.json"
        self.dataset_review: str = "datasets/yelp_review.json"

    @staticmethod
    def createSparkSession():
        return SparkSession \
            .builder \
            .appName("useCase") \
            .getOrCreate()
