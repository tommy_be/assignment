from pyspark.sql.dataframe import DataFrame
from pyspark.sql.functions import count, when, col
import matplotlib.pyplot as plt


class Stats(object):
    def __init__(self, sparkSession):
        self.sparkSession = sparkSession

    @staticmethod
    def countReviewsByAttribute(df: DataFrame) -> DataFrame:
        return df.select(
            count(when(df.useful > 0, df.useful)).alias("nbUsefulReviews"),
            count(when(df.funny > 0, df.funny)).alias("nbFunnyReviews"),
            count(when(df.cool > 0, df.cool)).alias("nbCoolReviews")
        )

    @staticmethod
    def countUsefulReviewsByCity(df: DataFrame) -> DataFrame:
        return df.select(
            count(when(df.useful > 0, df.useful)).alias("nbUsefulReviews"),
            count(when(df.funny > 0, df.funny)).alias("nbFunnyReviews"),
            count(when(df.cool > 0, df.cool)).alias("nbCoolReviews")
        )

    @staticmethod
    def countNullColumns(df: DataFrame) -> DataFrame:
        return df.select(
            [count(when(col(column).isNull(), column)).alias(f"nbNull{column}") for column in df.columns])

    @staticmethod
    def countReviewsByStars(df: DataFrame) -> DataFrame:
        return df \
            .groupBy("stars") \
            .count() \
            .withColumnRenamed("count", " nbOfReviews") \
            .sort("stars")

    @staticmethod
    def citiesByUsefulReviews(df: DataFrame) -> DataFrame:
        df = df.filter(col("useful") > 0)
        return df.groupBy("city").sum("review_count").sort("sum(review_count)", ascending=False)
