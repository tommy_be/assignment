import logging
import os

from pyspark.ml import Pipeline
from pyspark.sql import SparkSession
from pyspark.sql.dataframe import DataFrame
from pyspark.sql.functions import when, col

from configuration.configuration import Configuration, POSITIVE, NEGATIVE
from helpers.stats import Stats
from transformers.TransformRepository import TransformRepository

os.environ["OBJC_DISABLE_INITIALIZE_FORK_SAFETY"] = "YES"
LIMIT = 15000
CITY = "austin"
CATEGORY = "Restaurants"


class Application(object):

    def __init__(self):
        self.config = Configuration()
        self.spark: SparkSession = self.config.createSparkSession()
        self.stats = Stats(self.spark)
        self.repo = TransformRepository()

    def run(self):
        logging.info("Application start")
        df = self.__getDataFrame(city=CITY, limit=LIMIT)

        logging.info("Data exploration part")
        self.stats.citiesByUsefulReviews(df).show()
        self.stats.countReviewsByAttribute(df).show()
        self.stats.countNullColumns(df).show()
        self.stats.countReviewsByStars(df).show()

        logging.info("Simple ML part")

        dfRestaurantsOnly = df\
            .select(col("business_id"), col("name"), col("categories"), col("cityNormalized"), col("stars"), col("text"))\
            .filter(df.categories.contains(CATEGORY))

        pipeline = self.__simpleSentimentPipeline()

        # todo: down sample to have equal weight of positive and negative
        dfBinarized = dfRestaurantsOnly \
            .withColumn("binarized", when(dfRestaurantsOnly.stars > 3, POSITIVE).otherwise(NEGATIVE))

        # tuple like result
        trainData, testData = dfBinarized.randomSplit(weights=[0.8, 0.2])

        model = pipeline.fit(trainData)
        predict = model.transform(testData)
        predict.select("stars", "binarized", "probability", "prediction").show(100, truncate=False)

    def __loadJson(self, filename, limit=None) -> DataFrame:
        if limit is not None:
            return self.spark.read.json(filename).limit(limit)
        else:
            return self.spark.read.json(filename)

    def __getDataFrame(self, city=None, limit=None) -> DataFrame:
        df_business: DataFrame = self.__loadJson(self.config.dataset_business, limit)
        df_review: DataFrame = self.__loadJson(self.config.dataset_review, limit)

        if city is not None:
            if len(city.strip()):
                df_by_city = self.repo.singleStringNormalizer("city", "cityNormalized") \
                    .transform(df_business) \
                    .filter(col("cityNormalized") == city) \
                    .drop(col("stars"))

                return df_by_city.join(df_review, ["business_id"])

        else:
            return df_business\
                .drop(col("stars"))\
                .join(df_review, ["business_id"])

    def __simpleSentimentPipeline(self):
        return Pipeline(stages=[
            self.repo.regex("text", "wordsOnly"),
            self.repo.stopWords("wordsOnly", "wordsWithoutStopWords"),
            # self.repo.stemmer("wordsWithoutStopWords", "wordsStemmed),
            self.repo.vectorizer("wordsWithoutStopWords", "wordsVectorized"),
            self.repo.idf("wordsVectorized", "wordsIdf"),
            self.repo.logisticRegression(labelCol="binarized", featureCol="wordsIdf")
        ])


if __name__ == '__main__':
    Application().run()
