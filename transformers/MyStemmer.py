from nltk.stem.snowball import EnglishStemmer
from pyspark.ml import Transformer
from pyspark.sql.functions import udf
from pyspark.sql.types import StringType, ArrayType

stemmer = EnglishStemmer()


class MyStemmer(Transformer):

    def __init__(self, inputCol, outputCol):
        super(MyStemmer, self).__init__()
        self.inputCol = inputCol
        self.outputCol = outputCol

    @staticmethod
    def __stemmIt(word):
        return stemmer.stem(word)

    def _transform(self, dataset):
        transformUdf = udf(lambda words: [self.__stemmIt(word) for word in words], ArrayType(StringType()))(self.inputCol)
        return dataset.withColumn(self.outputCol, transformUdf)
