from pyspark.ml import Transformer
from pyspark.sql.functions import udf
from pyspark.sql.types import StringType, ArrayType


class SingleStringNormalizer(Transformer):
    def __init__(self, inputCol, outputCol):
        super().__init__()
        self.inputCol = inputCol
        self.outputCol = outputCol

    @staticmethod
    def __normalize(word: str) -> str:
        return word.lower().strip().replace(" ", "")

    def _transform(self, dataset):
        transformUdf = udf(lambda word: self.__normalize(word), StringType())(
            self.inputCol)
        return dataset.withColumn(self.outputCol, transformUdf)
