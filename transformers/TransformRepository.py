from pyspark.ml.classification import LogisticRegression
from pyspark.ml.feature import RegexTokenizer, StopWordsRemover, CountVectorizer, IDF

from transformers.MyStemmer import MyStemmer
from transformers.SingleStringNormalizer import SingleStringNormalizer


class TransformRepository(object):

    @staticmethod
    def singleStringNormalizer(inCol, outCol):
        return SingleStringNormalizer(inCol, outCol)

    @staticmethod
    def regex(inCol, outCol):
        return RegexTokenizer() \
            .setMinTokenLength(2) \
            .setInputCol(inCol) \
            .setOutputCol(outCol)

    @staticmethod
    def stopWords(inCol, outCol):
        return StopWordsRemover() \
            .setLocale("en_US") \
            .setInputCol(inCol) \
            .setOutputCol(outCol)

    @staticmethod
    def stemmer(inCol, outCol):
        return MyStemmer(inputCol=inCol, outputCol=outCol)

    @staticmethod
    def vectorizer(inCol, outCol):
        # undiscovered lands
        return CountVectorizer() \
            .setInputCol(inCol) \
            .setOutputCol(outCol)

    @staticmethod
    def idf(inCol, outCol):
        # a bit of more unknown
        return IDF() \
            .setInputCol(inCol) \
            .setOutputCol(outCol)

    @staticmethod
    def logisticRegression(labelCol, featureCol):
        # a bit of more unknown
        return LogisticRegression() \
            .setLabelCol(labelCol) \
            .setFeaturesCol(featureCol)
